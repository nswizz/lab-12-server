#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"
#include <sys/stat.h> // stat struct 

int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        fprintf(stderr, "You must enter in 2 filenames.\n");
        exit(1);
    }
    FILE *input = fopen(argv[1], "r");
    FILE *output = fopen(argv[2], "w");
    struct stat s1, s2;
    stat(argv[1], &s1);
    stat(argv[2], &s2);
    if(s1.st_ino == s2.st_ino)
    {
        fprintf( stderr, "You must enter 2 different files or you will lose information.\n");
        exit(3);
    }
    if(input == 0)
    {
        fprintf(stderr, "Can't open %s for reading\n", argv[1]);
        exit(4);
    }
    if(output == 0)
    {
        fprintf(stderr, "Can't open %s for reading\n", argv[2]);
        exit(4);
    }
    
    char line[100];
    while(fgets(line, 100, input)!= NULL)
    {
       char *hash = md5(line, strlen(line)-1);
       fprintf(output, "%s\n", hash);
       free(hash);
    }
    fclose(input);
    fclose(output);
    
}