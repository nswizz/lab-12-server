#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

char pop();
void push(char in);

char line[1500];
char rev[1500];
int max = 1500;
int top = 0;

int main( int argc, char *argv[] )
{
    if(argc < 2)
    {
        fprintf(stderr, "You must enter in 2 filenames.\n");
        exit(1);
    }
    
    FILE *input = fopen(argv[1], "r");
    FILE *output = fopen(argv[2], "w");
    
    if(input == 0)
    {
        fprintf(stderr, "Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    if(output == 0)
    {
        fprintf(stderr, "Can't open %s for reading\n", argv[2]);
        exit(1);
    }

    while(fgets(line, 1500, input) != NULL)
    {
        for(int i = 0; i < strlen(line); i++)
        {
            push(line[i]);
            if(line[i] == '\n')
            {
                while(top > 0)
                {
                    fprintf(output, "%c",pop(rev[top]));
                }
            }
        }
    }
    fprintf(output,"%s",line);
    fclose(input);
    fclose(output);
}

int isempty() 
{

   if(top == 0)
      return 1;
   else
      return 0;
}
   
int isfull() 
{

   if(top == max)
   {
       return 1;
   }
   else
   {
       return 0;
   }
      
}

char pop() 
{
   int out;
	
   if( !isempty() ) 
   {
      out = rev[top];
      top = top - 1;   
      return out;
   } 
   else
   {
       exit(1);
   }
}

void push(char in) 
{
   if( !isfull() ) 
   {
      top = top + 1;   
      rev[top] = in;
   } 
   else
   {
       exit(1);
   }
}
