#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main( int argc, char *argv[] )
{
    if(argc < 2)
    {
        fprintf(stderr, "You must enter in 2 filenames.\n");
        exit(1);
    }
    
    FILE *fp = fopen(argv[1], "r");
    if(fp == 0)
    {
        fprintf(stderr, "Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    int counter = 0;
    int c_count = 0;
    int word = 0;
    char input[1500];
    while(fgets(input, 1500, fp) != NULL)
    {
        for(int i = 0; i <= strlen(input); i++)
        {
            if( input[i] == ' ') 
            {
                c_count++;
            }
            if( isalnum(input[i]) ) 
            {
                c_count++;
            }
            if( ispunct(input[i]) ) 
            {
                c_count++;
            }
            
        }
        for(int z = 0; z <= strlen(input); z++)
        {
            for( int x = z+1; x <= strlen(input); x++)
            {
                if( input[x] == ' ' )
                {
                    word++;
                    z = x;
                }
                if( ispunct(input[x]) && input[x+1] == ' ' ) // ask barry about which punctuation he wants to be kept as making a word
                {
                    word++;
                    z = x;
                }
                if( input[x] == '\n' )
                {
                    word++;
                    z = x;
                }
            }
        }
        counter++;
    }
    fprintf(stdout,"Lines: %d\n", counter);
    fprintf(stdout, "Characters: %d\n", c_count);
    fprintf(stdout, "Words: %d\n", word);
    fclose(fp);
    
}