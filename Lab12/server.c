#include <stdio.h>
#include <stdlib.h>
#include "libsocket/libinetsocket.h"
#include <ctype.h>
#include <string.h>

FILE * connect_to_server();
void menu();
char get_choice();
void list_files(FILE *s);
void download(FILE *s);
void quit(FILE *s);
void handleChoice(char c, FILE *s);
int size(FILE *s, char file[]);

struct info
{
    char name[100];
};
int main()
{
    // Connect
    FILE * s = connect_to_server();
    
    // Menu
    menu();
    
    // Get choice
    char choice = get_choice();
    
    // Handle choice
    handleChoice(choice, s);
}

/*
 * Connect to server. Returns a FILE pointer that the
 * rest of the program will use to send/receive data.
 */
FILE * connect_to_server()
{
    int socknum = create_inet_stream_socket("runwire.com", "1234", LIBSOCKET_IPv4, 0);
    FILE *socket = fdopen(socknum, "r+");
    return socket;
}

/*
 * Display menu of choices.
 */
void menu()
{
    printf("    MENU     \n");
    printf("L List files\n");
    printf("D Download a file\n");
    printf("Q Quit\n");
    printf("\n");
}

/*
 * Get the menu choice from the user. Allows the user to
 * enter up to 100 characters, but only the first character
 * is returned.
 */
char get_choice()
{
    printf("Your choice: ");
    char buf[100];
    fgets(buf, 100, stdin);
    return buf[0];
}

/*
 * Display a file list to the user.
 */
void list_files(FILE *s)
{
    int size = 0;
    int count = 0;
    fprintf(s, "LIST\n");
    char filename[100], line[100];
    struct info file[15];
    fgets(line, 100, s);
    fgets(line, 100, s);
    while(fgets(line, 100, s) != NULL)
    {
        if(line[0] == '.')
        {
            break;
        }
        line[strlen(line)-1] = '\0';
        sscanf(line, "%d %s\n", &size, file[count].name);
        fprintf(stderr, "%d %s %d\n", count, file[count].name, size );
        
        count++;
    }
    printf("\n");
    
    menu();
    char choice = get_choice();
    handleChoice(choice, s);
    
}

/*
 * Download a file.
 * Prompt the user to enter a filename.
 * Download it from the server and save it to a file with the
 * same name.
 */
void download(FILE *s)
{
    printf("File Name to Download: ");
    char file[100];
    fgets(file, 100, stdin);
    int file_size = size(s, file);
    FILE *f = fopen(file, "w");
    
    fprintf(s, "GET %s", file);
    unsigned char line[100];
    int got;
    int want;
    int sofar = 0;
    fgets((char*)line, 100, s);
    while(1)
    {
        if(file_size - sofar < 100)
        {
            want = file_size - sofar;
        }
        else
        {
            want = 100;
        }
        got = fread(line, sizeof(unsigned char ), want, s);
        fwrite(line, sizeof(unsigned char ), got, f);
        sofar += got;
        if(sofar == file_size) 
        {
            
            break;
        }
    }
    fclose(f);
    
    menu();
    char choice = get_choice();
    handleChoice(choice, s);
}

/* 
 * Close the connection to the server.
 */
void quit(FILE *s)
{
    fclose(s);
}

void handleChoice(char c, FILE *s)
{
    // Handle choice
    switch(c)
    {
        case 'l':
        case 'L':
            list_files(s);
            break;
        
        case 'd':
        case 'D':
            download(s);
            break;
            
        case 'q':
        case 'Q':
            quit(s);
            exit(0);
            break;
            
        default:
            printf("Choice must be d, l, or q\n");
    }
}

int size(FILE *s, char *file)
{
    int size;
    char sizeLine[100];
    fprintf(s, "SIZE %s", file);
    fgets(sizeLine, 100, s);
    sscanf(sizeLine, "+OK %d\n", &size);
    int ret = sscanf(sizeLine, "+OK %d\n", &size);
    if(ret == 0)
    {
        fprintf(stderr, "Error Could Not Find File");
        menu();
        char choice = get_choice();
        handleChoice(choice, s);
    }
    return size;
}

